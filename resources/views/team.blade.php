@extends('layouts.header')

@section('content')

<div id="team">
	<section class="banner_area ">
		<div class="banner_inner overlay d-flex align-items-center">
			<div class="container">
				<div class="banner_content text-left">
					<div class="page_link">
						<a href="/">Home</a>
						<a href="/team">Team Members</a>
					</div>
					<h2>Team Members</h2>
				</div>
			</div>
		</div>
	</section>


	<section class="brand-area">
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-6">
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/ashish00.JPG" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/suman1212.JPG" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/nakul00.JPG" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/matina000.JPG" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/kiipananda000.jpg" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/suroj000.jpg" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/ritendra000.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="team-area section-gap">
		<div class="container member-s">


						<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suman2.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suman Mali</h4>
								<p>Director | Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/suman.mali.12"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Suman Mali, An engineer graduate from Bangalore Technological Institute, who has been working on the front-end development using html, css and javascript.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal2" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal2" tabindex="-1" role="dialog" aria-labelledby="team-membermodal2Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/suman2.JPG" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/suman.mali.12" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:rae@lucentbusiness.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Suman Mali</h4>
											<h2>Core Developer</h2>
											<p align="justify">Suman is a graduate degree holder of Bachelor of Engineering on Computer Science and Engineering from VTU University. His field of expertise is Website designing and development. </p>
											<p class="hide1" style="display: block; text-align: justify;"> He is dedicated to creating prompt, clean design of UI and UX. Suman has a strong work ethic and customer service and satisfaction record. He is a multitasker capable of bringing simultaneous web page creation and repair project to completion with full accuracy and efficiency.<br><br>Having an experience in JavaScript, Suman now enjoy programming in Laravel and Python. Suman enjoys his work and believes that enjoying work motivates him to be more creative and develop himself in every single project.<br><br>Besides his work, he enjoys the bike ride and explore different places and learn new things. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ashish1.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ashish Dulal</h4>
								<p>Designer | Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/ashishdulal052"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/coolboy_aziz"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ashish Dulal, A developer graduate from Ambition College, who has been working on the front-end development using Photoshop, Wordpress, Laravel and javascript.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal" href="#">View Details</a>
				</div>
			</div>
				<!-- Modal -->
			<div class="modal fade" id="team-membermodal" tabindex="-1" role="dialog" aria-labelledby="team-membermodalTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/ashish1.JPG" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/ashishdulal052" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="https://twitter.com/coolboy_aziz" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="https://www.instagram.com/ashish.aziz/" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:ashish@nepgeeks.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Ashish Dulal</h4>
											<h2>Creative Developer</h2>
											<p align="justify">Ashish Dulal, A developer graduate from Ambition College, who has been working on the front-end development using html, css, Wordpress and javascript.</p>
											<p class="hide1" style="display: block; text-align: justify;"> His abilities include being able to grasp how new technologies may be applied to help the company achieve its goals and analytical skills that help with strategic thinking and tactical implementation. His interpersonal skills are very good and he is able to create openness and trust among his colleagues. This has aided him in facilitation and conflict management to achieve a more effective working relationship in the office.
											<br><br>
											Highly-skilled software development professional bringing more than one year in software design, development and integration. Knowledgeable and skilled in data collection, analysis and management. Works well under pressure and consistently meets deadlines and targets while delivering high quality work.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			


			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/nakul4.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Nakul Budhathoki</h4>
								<p>Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/nakul.budhathoki"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Nakul Budhathoki, A developer graduate from Ambition College, who has been working on the Front-end development using Wordpress, Laravel and javascript.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal3" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal3" tabindex="-1" role="dialog" aria-labelledby="team-membermodal3Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/nakul4.JPG" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/nakul.budhathoki" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:rae@lucentbusiness.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Nakul Budhathoki</h4>
											<h2>Core Developer</h2>
											<p align="justify">Nakul Budhathoki, A developer graduate from Ambition College, who has been working on the Front-end development using Wordpress, Laravel and javascript.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is highly energetic and passionate individual who has achieved a Bachelors in CSIT. Nakul has a significant amount of experience as a developer for WordPress and Laravel.<br><br>
											Having worked with many large organisations he has now developed and honed his skills so that all projects undertaken are completed to the client’s requirements and strives to achieve 100% client satisfaction.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/Dipendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dipendra Manandhar</h4>
								<p>.Net Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dipen.manandhar.7"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dipendra Manandhar, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal4" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal4" tabindex="-1" role="dialog" aria-labelledby="team-membermodal4Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/Dipendra.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/dipen.manandhar.7" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:rae@lucentbusiness.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Dipendra Manandhar</h4>
											<h2>Core Developer</h2>
											<p align="justify">Dipendra Manandhar, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is dedicated to creating prompt, clean design of UI and UX. Dipendra has a strong work ethic and customer service and satisfaction record. He is a multitasker capable of bringing simultaneous web page creation and repair project to completion with full accuracy and efficiency.<br><br>Having an experience in JavaScript, Dipendra now enjoy programming in Laravel and Python. Dipendra enjoys his work and believes that enjoying work motivates him to be more creative and develop himself in every single project.<br><br>Besides his work, he enjoys the bike ride and explore different places and learn new things. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ritendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ritendra Dahal</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dahalritendra" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ritendra Dahal, a designer graduated from Kantipur Engineering College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermoda41" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermoda41" tabindex="-1" role="dialog" aria-labelledby="team-membermoda41Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/ritendra.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/dahalritendra" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:ritendra@nepgeeks.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Ritendra Dahal</h4>
											<h2>Graphics Designer</h2>
											<p align="justify">Ritendra Dahal, a designer graduated from Kantipur Engineering College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is skilled in illustrating his own ideas in satisfactory manner. Using various available design and knowledge to create what client need are few of his skills. A hardworking and team player member who is also skilled in front end development.<br><br>
											 Resourceful designer with the knowledge of modern trends and techniques. Completes the given task in given amount of time with satisfactory result. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suroj.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suroj Maharjan</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/surose.maharjan.7" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Suroj Maharjan, a designer graduated from Designing College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermoda411" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermoda411" tabindex="-1" role="dialog" aria-labelledby="team-membermoda41Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/suroj.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/surose.maharjan.7" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:suroj@nepgeeks.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Suroj Maharjan</h4>
											<h2>Graphics Designer</h2>
											<p align="justify">Suroj Maharjan, a designer graduated from Kantipur Engineering College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is skilled in illustrating his own ideas in satisfactory manner. Using various available design and knowledge to create what client need are few of his skills. A hardworking and team player member who is also skilled in front end development.<br><br>
											 Resourceful designer with the knowledge of modern trends and techniques. Completes the given task in given amount of time with satisfactory result. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/matina.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Matina Manandhar</h4>
								<p>Front Desk Officer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/matina.manandhar.5" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/ma_tee_na/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Matina Manandhar, front desk officer persuing Bachleors degree form Manmohan Memorial College,whos been handling overall work including finance and ecommerce.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermoda414" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermoda414" tabindex="-1" role="dialog" aria-labelledby="team-membermoda41Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/matina.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/matina.manandhar.5" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="instagram.com/ma_tee_na/" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:nilu.manan123@gmail.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Matina Manandhar</h4>
											<h2>Front Desk Officer</h2>
											<p align="justify">Matina Manandhar, front desk officer graduated Bachelors degree form Manmohan Memorial College,whos been handling overall work including finance and ecommerce.</p>
											<p class="hide1" style="display: block; text-align: justify;"> She is skilled in illustrating her own ideas in satisfactory manner. Using various available design and knowledge to create what client need are few of her skills. A hardworking and team player member who is also skilled in client handling.<br><br>
											 Resourceful content writer with the knowledge of modern trends and techniques. Completes the given task in given amount of time with satisfactory result. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/kiipananda.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Er. Kripananda Yadav</h4>
								<p>SEO Specialist</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/kripananda772" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/kripananda772/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Kripananda Yadav, SEO specialist graduated Bachleors degree form Kantipur Engineering College, whos been handling overall work including techinical SEO and local SE.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermoda4144" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermoda4144" tabindex="-1" role="dialog" aria-labelledby="team-membermoda41Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/kiipananda.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/kripananda772" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="instagram.com/kripananda772/" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:kripananda772@gmail.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Er. Kripananda Yadav</h4>
											<h2>SEO Specialist</h2>
											<p align="justify">Kripananda Yadav, SEO specialist persuing Bachleors degree form Kantipur Engineering College, whos been handling overall work including techinical SEO and local SE.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is organized and detail-oriented staff of adequate theoretical knowledge with excellent research and communication skills. Team player with initiative and intuitive to work on individual projects with enthusiasm to learn new things and always try to do his best in any work assigned and always tries to be appreciated as well-performer.<br><br>
												He help businesses in the domain of SEO Analysis, SEO Consultancy, SMM or Social Media Marketing, Copywriting, Brand Building, Reputation Management, online business strategies and online marketing strategies. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/Rajat-bhadra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Rajat Bhadra</h4>
								<p>Public Relation Officer, UK</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/bhadra.rajat"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Rajat Bhadra, Sales Executive who has been working for this company in London.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal5" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal5" tabindex="-1" role="dialog" aria-labelledby="team-membermodal5Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/Rajat-bhadra.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/bhadra.rajat" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:rae@lucentbusiness.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Rajat Bhadra</h4>
											<h2>Sales Executive</h2>
											<p align="justify">Rajat is an experienced, determined and confident professional and competent individual who has the enthusiasm and right skill set needed to ensure standards are met and everything gets done on time and within the budget.</p>
											<p class="hide1" style="display: block; text-align: justify;">Rajat is organized, ambitious, and possesses the capability to develop client portfolio. Having good communication skills means that he is more than able to construct a lucrative business for clients and advise accordingly.<br>
												<br>
											He is someone who goes out and gets what he wants rather than waiting for it to be brought to him. As a sociable person, he can hit the ground running, fit easily into any company environment and work closely with all level of hierarchy within a company to help their business grow.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/dhirendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dhirendra Kumar Yadav</h4>
								<p>Marketing Manager, Australia </p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dhirendrakumaryadav"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/dhirendrakumaryadav"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dhirendra is an experienced, determined and confident professional who has been working as a marketting Manager for this company in Australia (5 James Avenue Mitcham, 3132 , Victoria).
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermoda31" href="#">View Details</a>
				</div>
			</div>
			
				<!-- Modal -->
			<div class="modal fade" id="team-membermoda31" tabindex="-1" role="dialog" aria-labelledby="team-membermodalTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/dhirendra.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/dhirendrakumaryadav" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="https://twitter.com/dhirendrakumaryadav" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="https://www.instagram.com/dhirendrakumaryadav/" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:dhirendrakumaryadav@nepgeeks.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Dhirendra Kumar Yadav</h4>
											<h2>Marketing Manager, USA</h2>
											<p align="justify">Dhirendra Kumar Yadav, is an experienced, determined and confident professional and competent individual who has the enthusiasm and right skill set needed to ensure standards are met and everything gets done on time and within the budget.</p>
											<p class="hide1" style="display: block; text-align: justify;"> His educational carrier includes Bachelor In Civil Engineering( S.J.C INSTITUTE OF TECHNOLOGY, BANGALORE, INDIA) and Master of Engineering Management( CENTRAL QUEENSLAND UNIVERSITY, MELBOURNE, AUSTRALIA). He has Worked as an International Student Counsellor for S.J.C Institute of Technology(2014- Present).
											<br><br>
											Dhirendra controls all of the communication between a company and its customers. He manage internal teams in order to craft promotional messages, and they work to publish media across multiple channels.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/shyam-tamang.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Shyam Lal Tamang</h4>
								<p>.Net Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Shyam Lal Tamang, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal6" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal6" tabindex="-1" role="dialog" aria-labelledby="team-membermodal6Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/shyam-tamang.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:rae@lucentbusiness.com" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Shyam Lal Tamang</h4>
											<h2>.Net Developer</h2>
											<p align="justify">Shyam Lal Tamang, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is dedicated to creating prompt, clean design of UI and UX. Shyam has a strong work ethic and customer service and satisfaction record. He is a multitasker capable of bringing simultaneous web page creation and repair project to completion with full accuracy and efficiency.<br><br>Having an experience in JavaScript, Shyam now enjoy programming in Laravel and Python. Shyam enjoys his work and believes that enjoying work motivates him to be more creative and develop himself in every single project.<br><br>Besides his work, he enjoys the bike ride and explore different places and learn new things. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/RajuKArki.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Raju Karki</h4>
								<p>Public Relation Officer, USA</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/merazuu"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Raju Karki, A developing patner residing in United States Of America (USA).
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal7" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal7" tabindex="-1" role="dialog" aria-labelledby="team-membermodal7Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/RajuKArki.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/merazuu" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Raju Karki</h4>
											<h2>Sales Executive</h2>
											<p align="justify">Raju Karki, A developing patner residing in United States Of America (USA).</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is dedicated to creating prompt, clean design of UI and UX. Shyam has a strong work ethic and customer service and satisfaction record. He is a multitasker capable of bringing simultaneous web page creation and repair project to completion with full accuracy and efficiency.<br><br>Having an experience in JavaScript, Shyam now enjoy programming in Laravel and Python. Shyam enjoys his work and believes that enjoying work motivates him to be more creative and develop himself in every single project.<br><br>Besides his work, he enjoys the bike ride and explore different places and learn new things. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/AsheshDangal.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ashesh Dangol</h4>
								<p>Public Relation Officer, USA</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/ashes.dangol1"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ashesh Dangol, A developing patner residing in United States Of America (USA).
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal8" href="#">View Details</a>
				</div>
			</div>
			<div class="modal fade" id="team-membermodal8" tabindex="-1" role="dialog" aria-labelledby="team-membermodal8Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">NTPL</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="images/AsheshDangal.jpg" alt="my-image">
										</div>
										<ul class="portfolio-social">
											<li><a href="https://www.facebook.com/ashes.dangol1" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Ashesh Dangol</h4>
											<h2>Sales Executive</h2>
											<p align="justify">Ashesh Dangol, A developing patner residing in United States Of America (USA).</p>
											<p class="hide1" style="display: block; text-align: justify;"> He is dedicated to creating prompt, clean design of UI and UX. Shyam has a strong work ethic and customer service and satisfaction record. He is a multitasker capable of bringing simultaneous web page creation and repair project to completion with full accuracy and efficiency.<br><br>Having an experience in JavaScript, Shyam now enjoy programming in Laravel and Python. Shyam enjoys his work and believes that enjoying work motivates him to be more creative and develop himself in every single project.<br><br>Besides his work, he enjoys the bike ride and explore different places and learn new things. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>
@endsection