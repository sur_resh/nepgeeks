@extends('layouts.header')

@section('content')

<div id="project-details">
<section class="banner_area ">
<div class="banner_inner overlay d-flex align-items-center">
<div class="container">
<div class="banner_content text-left">
<div class="page_link">
<a href="/">Home</a>
<a href="/project">Projects</a>
<a href="/project-details">Details</a>
</div>
<h2>Our Projects</h2>
</div>
</div>
</div>
</section>


<section class="project-details section-gap">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6">
<div class="service-img my-img">
<img class="img-fluid" src="images/rapidresultsgourmet-co-uk.png" alt="">
</div>
</div>
<div class="col-lg-5 offset-lg-1 col-md-6">

<div class="single-details">
<h4>Rapid Results Gourmet</h4>
<p>Revolutionising nutrition in the U.K. Saving time, money and energy; all whilst improving productivity and well-being. We bridge the gap between Health and Wealth to unleash Great Britain's fullest potential!
</p>
<p>Here you can customise your meals exactly how you would like them prepared and cooked - down to the portion-size and level of spiciness!

To get started, simply create your RRG account and select how many meals a day you want below, and for how long you would like your meal plan to last. If you have to go on holiday, you can pause and restart your deliveries anytime.

Follow each prompt of selecting your meals and each of the sides. Once you’ve selected the maximum amount of meals for the meal plan you’ve chosen, you can then review your basket and make any changes you need to.

Give us any other requests, allergies and preferences and we’ll make sure your meals arrive to your door exactly how you want them. Don’t forget to let us know any specific delivery instructions so we can ensure we know where or who to drop your meals to! Easy as that 

Any questions? We’re only a call or message away and we will get back to you ASAP.

Happy Ordering!</p>
<ul class="list">
<li><span>Rating</span>: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></li>
<li><span>Client</span>: UK</li>
<li><span>Website</span>:rapidresultsgourmet.co.uk</li>
<li><span>Completed</span>: 10th Feb, 2019</li>
</ul>
<ul class="list social_details">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-behance"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</section>
</div>

@endsection