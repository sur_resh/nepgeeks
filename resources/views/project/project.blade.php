@extends('layouts.header')

@section('content')

<section class="banner_area ">
	<div class="banner_inner overlay d-flex align-items-center">
		<div class="container">
			<div class="banner_content text-left">
				<div class="page_link">
					<a href="/">Home</a>
					<a href="/project">Project</a>
				</div>
				<h2>Our Projects</h2>
			</div>
		</div>
	</div>
</section>


<section class="portfolio-area section-gap">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-12 col-md-12 project-side">
				<div class="active-gallery-carousel">

					<div class="single-gallery">
						<img class="img-fluid" src="images/rapidresultsgourmet-co-uk.png" alt="">
						<div class="gallery-content">
							<h4>Rapid Results Gourmet</h4>
							<p>Revolutionising nutrition in the U.K. Saving time, money and energy; all whilst improving productivity and well-being. We bridge the gap between Health and Wealth to unleash Great Britain's fullest potential!</p>
						</div>
						<div class="light-box">
							<a href="https://rapidresultsgourmet.co.uk/" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/pharmacysaver-lucentbusiness.png" alt="">
						<div class="gallery-content">
							<h4>Pharmacy Saver	</h4>
							<p>No medicine can be sold unless it has first been approved by the U.S. Food and Drug Administration (FDA). The makers of the medicine do tests on all new medicines and send the results to the FDA</p>
						</div>
						<div class="light-box">
							<a href="http://pharmacysaver.lucentbusiness.com/" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/myjourneynepal.png" alt="myjourneynepal">
						<div class="gallery-content">
							<h4>My Journey Nepal</h4>
							<p>My Journey is a service oriented organization with a group of experienced travel professionals and we are also Nepal's D M C. It is registered to Government of Nepal, Ministry of Culture and Tourism Department.</p>
						</div>
						<div class="light-box">
							<a href="http://myjourneynepal.com/" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/agfsconnect.png" alt="agfsconnect">
						<div class="gallery-content">
							<h4>AGFS Group UK</h4>
							<p>AGFS is led by CPA with 50 years of experience in private practice, has provided accounting and taxation services to thousands of clients throughout Australia and is designated as Managing Partner in leading accounting firm in Australia.</p>
						</div>
						<div class="light-box">
							<a href="http://agfsconnect.com.au" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/agfsconnect2.png" alt="agfsconnect2">
						<div class="gallery-content">
							<h4>AGFS Group Nepal</h4>
							<p>AGFS Nepal was founded and initiated by four founding partners, Anand Gautam, Amit Gautam, Machchhindra KC and Rishikesh Sapkota, coming together in a professional cluster.</p>
						</div>
						<div class="light-box">
							<a href="http://agfsconnect.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/rashcollection.png" alt="rashcollection">
						<div class="gallery-content">
							<h4>Rash Collection</h4>
							<p>Rash Collection is a bag shopping ecommerce site.</p>
						</div>
						<div class="light-box">
							<a href="http://rashcollection.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/plumbersnepal.png" alt="plumbersnepal">
						<div class="gallery-content">
							<h4>Plumbers Nepal</h4>
							<p>We provide plumbing services of the highest quality. We provide our communities, homes, and families with truly superior plumbing services so that they can safely and reliably use their plumbing systems.</p>
						</div>
						<div class="light-box">
							<a href="http://plumbersnepal.coming" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/hamronib.png" alt="hamronib">
						<div class="gallery-content">
							<h4>Hamro NIB (Nepalese in Bangalore)</h4>
							<p>NIB (Nepalese in Bangalore) is a mirror to all Nepalese residing in Bangalore. It reflects all the activities happening among Nepalese from Bangalore. NIB aims to unite all Nepalese who are in not only in Bangalore but also those who have left Bangalore. As a Whole we can say it as Sajha Chautari for entire Nepalese in Bangalore.</p>
						</div>
						<div class="light-box">
							<a href="http://hamronib.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/manforcegroup.png" alt="manforcegroup">
						<div class="gallery-content">
							<h4>Manforcegroup</h4>
							<p>Manforce is a leading company in diversified fields. We are experts in corporate outsourcing, construction and facility management. We focus on raising productivity through improved quality, efficiency and cost reductions across our total workforce.</p>
						</div>
						<div class="light-box">
							<a href="http://manforcegroup.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/aeritech.png" alt="aeritech">
						<div class="gallery-content">
							<h4>AeriTech</h4>
							<p>At AeriTech, we are driven to deliver custom software solutions to captivate users and create a technological boost to advance your business. We work closely with our customers to better meet their demands, that paired with our technical expertise creates an unparalleled product that is sure to exceed any expectations.</p>
						</div>
						<div class="light-box">
							<a href="http://aeritech.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/nepalesegifts.png" alt="nepalesegifts">
						<div class="gallery-content">
							<h4>Nepalese Gifts</h4>
							<p>An online gifts shop wit all the carts functionality.</p>
						</div>
						<div class="light-box">
							<a href="http://nepalesegifts.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/dianaoverseas.png" alt="dianaoverseas">
						<div class="gallery-content">
							<h4>Dianaoverseas</h4>
							<p>Diana Overseas is a thriving manpower recruitment company registered in the Ministry of Labour and Transport Management, Government of Nepal with License No. 408/060/061.Diana Overseas was established on 1st August, 2003 and has been providing overseas manpower recruitment services to our international clients in The Middle East and Far East. We are a pre-eminent ISO 9001:2015 certified company.</p>
						</div>
						<div class="light-box">
							<a href="http://dianaoverseas.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/mindstreamoutsourcing.png" alt="mindstreamoutsourcing">
						<div class="gallery-content">
							<h4>Mindstreamoutsourcing</h4>
							<p>Mind Stream, with a mission to build an innovative best in class people solutions global company that consistently delivering end to end high quality people services to its customers through best in class customer focus, business processes and a highly competent and committed team.</p>
						</div>
						<div class="light-box">
							<a href="http://mindstreamoutsourcing.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/progressivenaati.png" alt="progressivenaati">
						<div class="gallery-content">
							<h4>progressivenaati</h4>
							<p>Progressive Study Centre is a global platform for learning language and translation where students are mastering languages and achieving their goals by learning from courses taught by expert instructors. The online NAATI CCL course covers all aspects so you can access everything you need to succeed anywhere and anytime.</p>
						</div>
						<div class="light-box">
							<a href="http://progressivenaati.com.au" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/r2meducation.png" alt="r2meducation">
						<div class="gallery-content">
							<h4>r2meducation</h4>
							<p>R2M Education Specialists, one of the highly emerging face in the education consultancy industry, provides the students with transparent and interactive migration procedures. In the industry, where procedures are kept under a veil, R2M offers its students with easy processing and cooperative interaction in each and every steps. The students are not only guided but are accompanied toe-to-toe by our professional team.</p>
						</div>
						<div class="light-box">
							<a href="http://r2meducation.com" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/montessori-nepgeeks.png" alt="">
						<div class="gallery-content">
							<h4>MONTESSORI PORTFOLIO</h4>
							<p>A growing appreciation and demand for quality education and the remarkable personal touch that it offered saw a rapid expansion. The year 1990, saw the first batch of SLC students who excelled in academics; each passing year brought accolades, several rewarding and precious moments</p>
						</div>
						<div class="light-box">
							<a href="images/montessori-nepgeeks.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>

					<div class="single-gallery">
						<img class="img-fluid" src="images/hemitoindex.png" alt="">
						<div class="gallery-content">
							<h4>HEMITO PORTFOLIO</h4>
							<p>Hemito International is established since 2000 in Qatar. It is an international service management company with a skilled workforce of more than 1000 employees consisting both males and females with over 10 years of experience.</p>
						</div>
						<div class="light-box">
							<a href="images/hemitoindex.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>

					<div class="single-gallery">
						<img class="img-fluid" src="images/RK-HANDICRAFTS-PORTFOLIO.png" alt="">
						<div class="gallery-content">
							<h4>RK HANDICRAFTS PORTFOLIO</h4>
							<p>RK Fine arts and handicrafts is a prominent leader in Portraying and handirats. Since its establishment in 2000, Rk has been providing extraordianry portraying services</p>
						</div>
						<div class="light-box">
							<a href="images/RK-HANDICRAFTS-PORTFOLIO.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="testimonial-area section-gap-bottom">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 text-center">
				<div class="section-title">
					<p>Our Design & Development Services Provider</p>
					<h1>We Are <span>Nepgeeks.</span> Design & Development<br> <span>Services</span> Provider.</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-12">
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection