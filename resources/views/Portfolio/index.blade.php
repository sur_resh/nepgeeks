@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Portfolio</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('/admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active"><a href="#">Portfolio</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<section class='content'>
	<div class='container-fluid'>
		<p>
			<a href="/portfolio/create" class="btn btn-primary">Add new portfolio</a>
		</p>
		
		<table class="table table-bordered table-stripped">
	        <tr>
	        	<th>ID</th>
	        	<th>title</th>
	        	<th>description</th>
	        	  	<th>link</th>
	        	<th>Action</th>
	        	<th>image</th>
	        </tr>
	        @foreach($portfolio as $p)
	        <tr>
	        	<td>{{$p->id}}</td>
	        		<td>{{$p->title}}</td>
	        		<td>{{$p->description}}</td>
	        		<td>{{$p->link}}</td>
	        		        		
	        			<td><a href="/portfolio/edit/{{$p->id}}" class="btn btn-info">Edit</a>   &nbsp;   <a href="/portfolio/delete/{{$p->id}}" class="btn btn-danger">Delete</a></td>
	        <td>{{$p->image}}</td>
	        </tr>
	        @endforeach
         </table>
     
	   
	     
	</div>
</section>

@endsection