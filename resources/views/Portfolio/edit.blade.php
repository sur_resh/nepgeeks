@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Edit portfolio</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('/admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active"><a href="#">portfolio</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<section class='content'>
	<div class='container-fluid'>
		<form method="POST" action="/portfolio/edit/{{$portfolio->id}}" enctype="multipart/form-data">
			{{ csrf_field() }}

				<div class="form-group">
					<input type="text" name="title"  value="{{$portfolio->title}}"  class="form-control" required />
				</div>
				<div class="form-group">
				<input type="text" name="description"  value="{{$portfolio->description}}"  class="form-control" required />
			</div>
			<div class="form-group">
				<input type="text" name="link"  value="{{$portfolio->link}}"  class="form-control" required />
			</div>
				
				<div class="form-group">
					<input type="file" name="image" value="{{ asset('/images/'.$portfolio->image )}}	" accept="images/png, images/jpg, images/jpeg">
					<img src="{{ asset('/images/'.$portfolio->image )}}" style="width:150px;">
				</div>
				<div>
					<button type='submit' class='button is-link'>Edit</button>
				</div>.

			</form>

		</div>
	</section>

	@endsection