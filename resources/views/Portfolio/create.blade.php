@extends('layouts.admin')
@section('content')

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Create portfolio</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('/admin') }}">Dashboard</a></li>
					<li class="breadcrumb-item active"><a href="#"> portfolio</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<section class='content'>
	<div class='container-fluid'>
		<form method="POST" action="/portfolio/create" enctype="multipart/form-data">
			{{ csrf_field() }}
			
			<div class="form-group">
				<input type="text" name="title"  placeholder="title" class="form-control" required />
			</div>
           <div class="form-group">
				<input type="text" name="description"  placeholder="description" class="form-control" required />
			</div>
			<div class="form-group">
				<input type="text" name="link"  placeholder="link" class="form-control" required />
			</div>
			<div class="form-group">
				<input type="file" name="image" accept="image/jpg, image/png, image/jpeg">

								</div>
			</div>
			<div>
				<button type='submit' class='button is-link'>Save</button>
			</div>


		</form>

	</div>
</section>

@endsection