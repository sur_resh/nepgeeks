@extends('layouts.header')

@section('content')


<section class="banner_area ">
	<div class="banner_inner overlay d-flex align-items-center">
		<div class="container">
			<div class="banner_content text-left">
				<div class="page_link">
					<a href="/">Home</a>
					<a href="/about">About Us</a>
				</div>
				<h2>About Us</h2>
			</div>
		</div>
	</div>
</section>


<section class="video-sec-area">
	<div class="container">
		<div class="row justify-content-start align-items-center">
			<div class="col-lg-6 video-left justify-content-center align-items-center d-flex">
				<div class="overlay overlay-bg"></div>
				<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?time_continue=2&v=J9YzcEe29d0">
					<span></span>
				</a>
			</div>
			<div class="col-lg-5 offset-lg-1 video-right">
				<h1>We Are Nepgeeks. <br>
				Some Info About Us</h1>
				<p>
					Web development services that we provide are from simple static pages to dynamic pages which are optimized, secure, search engine friendly and pass both html & css validation.
				</p>
				<div class="counter_area" id="project_counter">

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">50</span>+</h4>
							<p>Live Websites</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">1000</span>+</h4>
							<p>Logo Designs</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">500</span>+</h4>
							<p>Trusted Clients</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">20</span>+</h4>
							<p>Developers</p>
						</div>
					</div>
				</div>
			</div>
			<img class="img-fluid video-shape" src="images/video-bg-shape.png" alt="">
		</div>
	</div>
</section>


<section class="cta-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-12">
				<h2>Get to Know Project Estimate?</h2>
				<p>There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s
				exciting to think about setting up your own viewing station whether that is on the deck</p>
				<a href="/contact" class="primary-btn">Get Estimate for Free</a>
			</div>
			<img src="images/cta-bg-shape.png" alt="" class="cta-shape img-fluid">
		</div>
	</div>
</section>


<section class="team-area section-gap">
	<div class="container">
		<div class="owl-carousel active-team-carusel">

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ashish1.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ashish Dulal</h4>
								<p>Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Ashish Dulal, A developer graduate from Ambition College, who has been working on the front-end development using html, css, Wordpress and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suman2.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suman Mali</h4>
								<p>Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Suman Mali, An engineer graduate from Bangalore Technological Institute, who has been working on the front-end development using html, css and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/Dipendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dipendra Manandhar</h4>
								<p>.Net Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dipen.manandhar.7"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dipendra Manandhar, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.
					</p>
					<a class="primary-btn" data-toggle="modal" data-target="#team-membermodal4" href="#">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/nakul4.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Nakul Budhathoki</h4>
								<p>Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Nakul Budhathoki, A developer graduate from Ambition College, who has been working on the Back-end development using Laravel and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ritendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ritendra Dahal</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dahalritendra" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ritendra Dahal, a designer graduated from Kantipur Engineering College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/kiipananda.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Er. Kripananda Yadav</h4>
								<p>SEO Specialist</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/kripananda772" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/kripananda772/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Kripananda Yadav, SEO specialist graduated Bachleors degree form Kantipur Engineering College, whos been handling overall work including techinical SEO and local SE.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suroj.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suroj Maharjan</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/surose.maharjan.7" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Suroj Maharjan, a designer graduated from Designing College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/matina.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Matina Manandhar</h4>
								<p>Front Desk Officer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/matina.manandhar.5" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/ma_tee_na/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Matina Manandhar, front desk officer persuing Bachleors degree form Manmohan Memorial College,whos been handling overall work including finance and ecommerce.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/dhirendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dhirendra Kumar Yadav</h4>
								<p>Marketing Manager, Australia </p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dhirendrakumaryadav"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/dhirendrakumaryadav"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dhirendra is an experienced, determined and confident professional who has been working as a marketting Manager for this company in Australia (5 James Avenue Mitcham, 3132 , Victoria).
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="testimonial-area section-gap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 text-center">
				<div class="section-title">
					<p>Our Design & Development Services Provider</p>
					<h1>We Are <span>Nepgeeks.</span> Design & Development<br> <span>Services</span> Provider.</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-12">
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection