@extends('layouts.header')

@section('content')

<section class="banner_area ">
	<div class="banner_inner overlay d-flex align-items-center">
		<div class="container">
			<div class="banner_content text-left">
				<div class="page_link">
					<a href="/">Home</a>
					<a href="/service">Services</a>
				</div>
				<h2>Our Services</h2>
			</div>
		</div>
	</div>
</section>
<section class="brand-area">
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-6 section-title">
					<h2><span>Our</span> Areas Of Expertise</h2>
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/illustrator.png" alt="illustrator">
								<br>
								<h4>Illustrator</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/laravellogo.png" alt="laravel">
								<br>
								<h4>Laravel</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/photoshop-express.png" alt="photoshop-express">
								<br>
								<h4>Photoshop</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/php-file.png" alt="php">
								<br>
								<h4>Php</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/seo-64.png" alt="seo">
								<br>
								<h4>Seo</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/wordpress-64.png" alt="wordpress">
								<br>
								<h4>Wordpress</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/vue-js.png" alt="vue-js">
								<br>
								<h4>Vue Js</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/indesign.png" alt="indesign">
								<br>
								<h4>In Design</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/woocommerce.png" alt="woocommerce">
								<br>
								<h4>Woo Commerce</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/javascripticon.png" alt="javascripticon">
								<br>
								<h4>JAva Script</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/css_logo4.png" alt="css_logo4">
								<br>
								<h4>CSS</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/htmlicon.png" alt="htmlicon">
								<br>
								<h4>HTML</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/ionic-logo-portrait.png" alt="ionic-Web-Development">
								<br>
								<h4>Ionic</h4>
							</div>
						</div>
							<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/Python-Logo.png" alt="Python-Logo">
								<br>
								<h4>Python</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<section class="service-area section-gap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 text-center">
				<div class="section-title">
					<p>We Are Providing these services</p>
					<h1>We Are <span>Nepgeeks.</span> We Provide These <br> Services to Our Customers</h1>
				</div>
			</div>
		</div>
		<div class="row make-center">

			<div class="col-lg-4 col-sm-6 col-md-6">
				<div class="single-service">
					<div class="service-icon">
						<img src="images/web-designing-opt.jpg" alt="">
					</div>
					<div class="service-content">
						<h4>Web Design & Development</h4>
						<p>We provide affordable web design and development service in Nepal. Web development services include creating websites for your organization to introduce your business or services online.We also design and develop  E-commerce websites which have user-friendly and responsive design.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-sm-6 col-md-6">
				<div class="single-service">
					<div class="service-icon">
						<img src="images/Graphic-Design.jpeg" alt="">
					</div>
					<div class="service-content">
						<h4>Graphics Design</h4>
						<p>The designer works with a variety of communication tools in order to convey a message from a client to a particular audience. Which includes logo for your brand or website, promotion banner, visiting card and many more.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-sm-6 col-md-6">
				<div class="single-service">
					<div class="service-icon">
						<img src="images/seo-graph.jpg" alt="">
					</div>
					<div class="service-content">
						<h4>Digital Marketing Service</h4>
						<p>We help you grow your business by expanding your reachability and connect to your potential customers using different platforms like Facebook, Instagram. We also provide SEO service to rank your website in different search results to increase your traffic and eventually sales of your product and service.</p>
					</div>
				</div>
			</div>
			<div class="row make-gap make-center">

				<div class="col-lg-4 col-sm-6 col-md-6">
					<div class="single-service">
						<div class="service-icon">
							<img src="images/hosting.jpg" alt="">
						</div>
						<div class="service-content">
							<h4>Domain & Hosting</h4>
							<p>We will help you choose a domain name which will represent your brand or product and register on your behalf. We also provide affordable hosting plans that can fit your requirements.</p>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-sm-6 col-md-6">
					<div class="single-service">
						<div class="service-icon">
							<img src="images/ecommerce.jpg" alt="">
						</div>
						<div class="service-content">
							<h4>Mobile App Development</h4>
							<p>Nepgeeks specializes in building mobile apps for all platforms like ios, Android. Which helps your customer to access all your services remotely.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-sm-6 col-md-6">
					<div class="single-service">
						<div class="service-icon">
							<img src="images/wordpress-customization.jpg" alt="">
						</div>
						<div class="service-content">
							<h4>Customization</h4>
							<p>Development is at the core of our business at Nepgeeks. is the world's no.1 Content Management System (CMS) and technically secure CMS on the Market.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="testimonial-area section-gap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 text-center">
				<div class="section-title">
					<p>Our Design & Development Services Provider</p>
					<h1>We Are <span>Nepgeeks.</span> Design & Development<br> <span>Services</span> Provider.</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-12">
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection