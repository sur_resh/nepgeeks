@extends('layouts.header')

@section('content')

<section class="banner_area ">
	<div class="banner_inner overlay d-flex align-items-center">
		<div class="container">
			<div class="banner_content text-left">
				<div class="page_link">
					<a href="/">Home</a>
					<a href="/contact">Contact Us</a>
				</div>
				<h2>Contact Us</h2>
			</div>
		</div>
	</div>
</section>


<section class="contact_area section-gap">
	<div class="container">
		<div class="bottom-space">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.462414699711!2d85.3212034146198!3d27.70300558279364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a85bffffff%3A0x358c8c57ee9eb1ad!2sNepgeeks+Technology!5e0!3m2!1sen!2snp!4v1542624116348" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<div class="contact_info">
					<div class="info_item">
						<i class="lnr lnr-home"></i>
						<h6>Putalisadak, Kathmandu</h6>
						<p>Nepal</p>
					</div>
					<div class="info_item">
						<i class="lnr lnr-phone-handset"></i>
						<h6><a href="tel:+977014011101">014011101</a></h6>
						<p>Sun to Fri 9am to 6 pm</p>
					</div>
					<div class="info_item">
						<i class="lnr lnr-envelope"></i>
						<h6><a href="mailto:info@nepgeeks.com"><span class="__cf_email__" data-cfemail="info@nepgeeks.com">info@nepgeeks.com</span></a></h6>
						<p>Send us your query anytime!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
				<form class="row contact_form" action="/sendemail/send" method="post" id="contactForm" novalidate="novalidate">
{{csrf_field()}}
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
						</div>
						<div class="form-group">
							<input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter phone number" pattern="[0-9]{10}" aria-describedby="phoneHelp">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea class="form-control" name="message" id="message" rows="7" placeholder="Enter Message"></textarea>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" value="submit" class="primary-btn">Send Message</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection