<footer class="footer-area section_gap">
	<div class="container">
		<div class="row footer-top">
			<div class="col-lg-3  col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h6>About Nepgeeks</h6>
					<p>We provide serives including Graphic design, Website design, development, Search Engine Optimization,digital marketing, Google Adword and social media marketing.</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h6>Navigation Links</h6>
					<div class="row">
						<ul class="col footer-nav">
							<li><a href="/">Home</a></li>
							<li><a href="/service
								">Services</a></li>
								<li><a href="project
									">Project</a></li>
								</ul>
								<ul class="col footer-nav">
									<li><a href="/team
										">Team Members</a></li>
										<li><a href="/blog
											">Blog</a></li>
											<li><a href="contact
												">Contact</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-3  col-md-6 col-sm-6">
									<div class="single-footer-widget">
										<h6>Newsletter</h6>
										<p>We're Building the World of Technology!</p>
										<div class="" id="subscribe">
@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
											<form novalidate="true" action="/sendmail/subscribe" method="post" class="form-inline">
{{csrf_field()}}
												<div class="d-flex flex-row">
													<input class="form-control" name="email" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
													<button type="submit" class="click-btn btn btn-default">
														<i class="fa fa-paper-plane" aria-hidden="true"></i>
													</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-lg-3  col-md-6 col-sm-6">
									<div class="single-footer-widget mail-chimp">
										<h6 class="">Contact Us</h6>
										<div class="contact_info">
											<div class="info_item">
												<i class="lnr lnr-home"></i>
												<h6>Putalisadak, Kathmandu</h6>
												<p>Nepal</p>
											</div>
											<div class="info_item">
												<i class="lnr lnr-phone-handset"></i>
												<h6><a href="tel:+977014011101">014011101</a></h6>
												<p>Sun to Fri 9am to 6 pm</p>
											</div>
											<div class="info_item">
												<i class="lnr lnr-envelope"></i>
												<h6><a href="mailto:info@nepgeeks.com"><span class="__cf_email__" data-cfemail="info@nepgeeks.com">info@nepgeeks.com</span></a></h6>
												<p>Send us your query anytime!</p>
											</div>
										</div>
										<div class="footer-social d-flex align-items-center">
											<a href="https://www.facebook.com/nepgeeksTechnology/" target="_blank" ><i class="fa fa-facebook"></i></a>
											<a href="https://twitter.com/nepgeeks"><i class="fa fa-twitter"></i></a>
											<a href="https://www.instagram.com/nepgeeks_technology/" target="_blank" ><i class="fa fa-instagram"></i></a>
											<a href="https://www.youtube.com/channel/UCncISlvstAM4vtb6YRyHf5A" target="_blank" ><i class="fa fa-youtube-square"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
							<div class="container">
								<div class="row justify-content-between">
									<div class="col-lg-12">
										<div>
											<p class="footer-text m-0">
												Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://nepgeeks.com" target="_blank">Nepgeeks Technology</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</footer>
