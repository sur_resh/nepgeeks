<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="shortcut icon" href="img/fav.png">

	<meta name="author" content="Nepgeeks Technology">

	<meta name="description" content="We are a team of web professionals serving for the clients from different country. We work as a team for web designing and web application development, search engine optimization, e-commerce solution and other web related services.">

	<meta name="keywords" content="Nepgeeks,SEO">

	<meta charset="UTF-8">

	<title>Nepgeeks Technology</title>
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700|Roboto:400,400i,500" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="js/jquery-2.2.4.min.js"></script>
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/jquery.magnific-popup.min.js"></script>
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/hexagons.min.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="/assets/css/main.css">
	<link rel="stylesheet" href="/assets/css/animate.css">

</head>
<body>
	    <!-- header-section start  -->
    <header class="header-section">
      <div class="header-bottom">
        <div class="container">
          <nav class="navbar navbar-expand-xl align-items-center">
            <a class="site-logo site-title" href="/"><img src="images/nepgeeks-logo.png" alt="site-logo"><span class="logo-icon"><i class="flaticon-fire"></i></span></a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="menu-toggle"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav main-menu ml-auto">
                <li><a href="/" class="{{ Request::is('/') ? 'active' : '' }}">Home <div class="active-line"></div></a></li>
                <li><a href="/about" class="{{ Request::is('about') ? 'active' : '' }}">about&nbsp;us <div class="active-line"></div></a></li>
                <li><a href="/service" class="{{ Request::is('service') ? 'active' : '' }}">Services <div class="active-line"></div></a></li>
                <li><a href="/project" class="{{ Request::is('project') ? 'active' : '' }}">Projects <div class="active-line"></div></a></li>
                <li><a href="/team" class="{{ Request::is('team') ? 'active' : '' }}">Our&nbsp;Team <div class="active-line"></div></a></li>
                <li><a href="/blog" class="{{ Request::is('blog') ? 'active' : '' }}">Blog <div class="active-line"></div></a></li>
<!--                 <li class="menu_has_children"><a href="#0">Blog</a>
                  <ul class="sub-menu">
                    <li><a href="blogs">Blog</a></li>
                    <li><a href="blog-details">Blog Details</a></li>
                  </ul>
                </li> -->
                <li><a href="/contact" class="{{ Request::is('contact') ? 'active' : '' }}">contact <div class="active-line"></div></a></li>
              </ul>
            </div><!-- navbar-collapse end -->
          </nav>
        </div>
      </div>
    </header>
    <!-- header-section end  -->
	<div class="side_menu">
	<div class="logo">
		<a href="/">
			<img src="images/nepgeeks-logo.png" alt="">
		</a>
	</div>
	<ul class="list menu-left">
		<li>
			<a href="/" class="{{ Request::is('/') ? 'active' : '' }}">Home</a>
		</li>
		<li>
			<a href="/about" class="{{ Request::is('about') ? 'active' : '' }}">About Us</a>
		</li>
		<li>
			<a href="/service" class="{{ Request::is('service') ? 'active' : '' }}">Services</a>
		</li>
		<!-- <li>
			<div class="dropdown">
				<button type="button" class="dropdown-toggle" data-toggle="dropdown">
					Projects
				</button>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="/project">Project</a>
					<a class="dropdown-item" href="/project-details">Project Details</a>
				</div>
			</div>
		</li> -->
		<li>
			<a href="/project" class="{{ Request::is('project') ? 'active' : '' }}">Projects</a>
		</li>
		<li>
			<a href="/team" class="{{ Request::is('team') ? 'active' : '' }}">Our Team</a>
		</li>
		<li>
			<a href="/blog" class="{{ Request::is('blog') ? 'active' : '' }}">Blog</a>
		</li>
		<li>
			<a href="/contact" class="{{ Request::is('contact') ? 'active' : '' }}">Contact Us</a>
		</li>
	</ul>
</div>


<div class="canvus_menu">
	<div class="container">
		<div class="toggle_icon" title="Menu Bar">
			<span></span>
		</div>
	</div>
</div>

	@yield('content')
	@include('layouts.footer')


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>

	<script src="js/parallax.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/hexagons.min.js"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/main.js"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>
<script type="text/javascript">
	 // mobile menu js
  $(".navbar-collapse>ul>li>a, .navbar-collapse ul.sub-menu>li>a").on("click", function() {
    const element = $(this).parent("li");
    if (element.hasClass("open")) {
      element.removeClass("open");
      element.find("li").removeClass("open");
    }
    else {
      element.addClass("open");
      element.siblings("li").removeClass("open");
      element.siblings("li").find("li").removeClass("open");
    }
  });

  //js code for menu toggle
  $(".menu-toggle").on("click", function() {
      $(this).toggleClass("is-active");
  });
  
   // menu options custom affix
   var fixed_top = $(".header-section");
   $(window).on("scroll", function(){
       if( $(window).scrollTop() > 50){  
           fixed_top.addClass("animated fadeInDown menu-fixed");
       }
       else{
           fixed_top.removeClass("animated fadeInDown menu-fixed");
       }
   });
</script>
</body>
</html>
