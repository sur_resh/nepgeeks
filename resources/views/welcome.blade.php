@extends('layouts.header')

@section('content')

<section class="home-banner-area">
	<div class="container">
		<div class="row justify-content-end fullscreen">
			<div class="col-lg-6 col-md-12 home-banner-left d-flex fullscreen align-items-center">
				<div>
					<h1 class="">
						We Are <span>Nepgeeks</span>. <br>
						SEO Friendly Web Design & Digital Marketing
						<span>Service</span> Provider.
					</h1>
					<p class="mx-auto mb-40">
						We are a team of web professionals serving for the clients from different country. We work as a team for web designing and web application development, search engine optimization, e-commerce solution and other web related services.
					</p>
					<a href="/about" class="primary-btn">More About Us</a>
<!-- 					<div class="d-flex align-items-center mt-60">
						<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=vParh5wE-tM">
							<span></span>
						</a>
						<div class="watch_video">
							<h5>Watch Live Demo</h5>
							<p>You will love our execution</p>
						</div>
					</div> -->
				</div>
			</div>
			<div class="col-lg-6 col-md-12 no-padding home-banner-right d-flex fullscreen align-items-end">
				<img class="img-fluid" src="images/header-img.png" alt="">
			</div>
		</div>
			<div class="row justify-content-start">
				<div class="col-lg-6 section-title">
					<h2><span>Our</span> Areas Of Expertise</h2>
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/illustrator.png" alt="illustrator">
								<br>
								<h4>Illustrator</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/laravellogo.png" alt="laravel">
								<br>
								<h4>Laravel</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/photoshop-express.png" alt="photoshop-express">
								<br>
								<h4>Photoshop</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/php-file.png" alt="php">
								<br>
								<h4>Php</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/seo-64.png" alt="seo">
								<br>
								<h4>Seo</h4>
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/wordpress-64.png" alt="wordpress">
								<br>
								<h4>Wordpress</h4>
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/vue-js.png" alt="vue-js">
								<br>
								<h4>Vue Js</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/indesign.png" alt="indesign">
								<br>
								<h4>In Design</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/woocommerce.png" alt="woocommerce">
								<br>
								<h4>Woo Commerce</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/javascripticon.png" alt="javascripticon">
								<br>
								<h4>JAva Script</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/css_logo4.png" alt="css_logo4">
								<br>
								<h4>CSS</h4>
							</div>
						</div>
						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/htmlicon.png" alt="htmlicon">
								<br>
								<h4>HTML</h4>
							</div>
						</div>
						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/ionic-logo-portrait.png" alt="ionic-Web-Development">
								<br>
								<h4>Ionic</h4>
							</div>
						</div>
							<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/Python-Logo.png" alt="Python-Logo">
								<br>
								<h4>Python</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<section class="service-area section-gap1">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 text-center">
				<div class="section-title">
					<h1>We Are <span>Nepgeeks.</span> Our Areas <br>Of Expertise</h1>
				</div>
			</div>
		</div>

		<div class="row blog-inner justify-content-center">

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/Graphic-Design.jpeg" alt="Graphic-Design">
					</div>
					<div class="blog-details">
						<h4>Web Design & Development</h4>
						<p>We provide affordable web design and development service in Nepal. Web development services include creating websites for your organization to introduce your business or services online.We also design and develop  E-commerce websites which have user-friendly and responsive design.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/seo-graph.jpg" alt="seo">
					</div>
					<div class="blog-details">
						<h4>Digital Marketing Service</h4>
						<p>We help you grow your business by expanding your reachability and connect to your potential customers using different platforms like Facebook, Instagram. We also provide SEO service to rank your website in different search results to increase your traffic  and eventually sales of your product and service.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid"src="images/ecommerce.jpg" alt="ecommerce">

						</div>
						<div class="blog-details">
							<h4>E-commerce Web Design</h4>
							<p>We can help you design and develop e-commerce solutions to data and content management, shopping cart applications, and payment gateway integration.</p>
						</div>
					</div>
				</div>

		<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/hosting.jpg" alt="domain & hosting">
						</div>
						<div class="blog-details">
							<h4>Domain & Hosting</h4>
							<p>We will help you choose a domain name which will represent your brand or product and register on your behalf. We also provide affordable hosting plans that can fit your requirements.</p>
						</div>
					</div>
				</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/wordpress-customization.jpg" alt="wordpress">
						</div>
						<div class="blog-details">
							<h4>Customization</h4>
							<p>Development is at the core of our business at Nepgeeks. is the world's no.1 Content Management System (CMS) and technically secure CMS on the Market.</p>
						</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/Graphic-Design.jpeg" alt="Graphic-Design">
					</div>
					<div class="blog-details">
						<h4>Graphics Design</h4>
						<p>The designer works with a variety of communication tools in order to convey a message from a client to a particular audience. Which includes logo for your brand or website, promotion banner, visiting card and many more.</p>
					</div>
			</div>
		</div>
							<a href="/service" class="primary-btn">View More</a>
		</div>
	</div>
</section>
<section class="cta-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-12">
				<h2>Get to Know Project Estimate?</h2>
				<p>We have extensive experience in building applications ranging from web-based to mobile platform. We specialize in Web, Social Networking and Mobile platforms. </p>
				<a href="/contact" class="primary-btn">Get Estimate for Free</a>
			</div>
			<img src="images/cta-bg-shape.png" alt="" class="cta-shape img-fluid">
		</div>
	</div>
</section>

<section class="video-sec-area">
	<div class="container">
		<div class="row justify-content-start align-items-center">
			<div class="col-lg-6 video-left justify-content-center align-items-center d-flex">
				<div class="overlay overlay-bg"></div>
				<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=LjikdCV0aV8">
					<span></span>
				</a>
			</div>
			<div class="col-lg-5 offset-lg-1 video-right">
				<h1>We Are Nepgeeks. <br>
				Some Info About Us</h1>
				<p>
					Web development services that we provide are from simple static pages to dynamic pages which are optimized, secure, search engine friendly and pass both html & css validation.
				</p>
				<div class="counter_area" id="project_counter">

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">50</span>+</h4>
							<p>Live Websites</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">1000</span>+</h4>
							<p>Logo Designs</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">500</span>+</h4>
							<p>Trusted Clients</p>
						</div>
					</div>

					<div class="single_counter">
						<div class="info-content">
							<h4><span class="counter">20</span>+</h4>
							<p>Developers</p>
						</div>
					</div>
				</div>
			</div>
			<img class="img-fluid video-shape" src="images/video-bg-shape.png" alt="">
		</div>
	</div>
</section>


<section class="portfolio-area section-gap">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-lg-6 col-md-6">
				<div class="portfolio-main">
					<h1>Recent Portfolio <br> Showcase</h1>
					<p>We are a team of web professionals serving for the clients from different country. We work as a team for web designing and web application development, search engine optimization, e-commerce solution and other web related services.</p>
					<a href="/project" class="primary-btn">View all Works</a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="active-gallery-carousel">

					<div class="single-gallery">
						<img class="img-fluid" src="images/rapidresultsgourmet-co-uk.png" alt="">
						<div class="gallery-content">
							<h4>Rapid Results Gourmet</h4>
							<p>Revolutionising nutrition in the U.K. Saving time, money and energy; all whilst improving productivity and well-being. We bridge the gap between Health and Wealth to unleash Great Britain's fullest potential!</p>
						</div>
						<div class="light-box">
							<a href="https://rapidresultsgourmet.co.uk/" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/pharmacysaver-lucentbusiness.png" alt="">
						<div class="gallery-content">
							<h4>Pharmacy Saver</h4>
							<p>No medicine can be sold unless it has first been approved by the U.S. Food and Drug Administration (FDA). The makers of the medicine do tests on all new medicines and send the results to the FDA</p>
						</div>
						<div class="light-box">
							<a href="http://pharmacysaver.lucentbusiness.com/" target="_blank">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
					<div class="single-gallery">
						<img class="img-fluid" src="images/montessori-nepgeeks.png" alt="">
						<div class="gallery-content">
							<h4>MONTESSORI PORTFOLIO</h4>
							<p>A growing appreciation and demand for quality education and the remarkable personal touch that it offered saw a rapid expansion. The year 1990, saw the first batch of SLC students who excelled in academics; each passing year brought accolades, several rewarding and precious moments</p>
						</div>
						<div class="light-box">
							<a href="images/montessori-nepgeeks.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>

					<div class="single-gallery">
						<img class="img-fluid" src="images/hemitoindex.png" alt="">
						<div class="gallery-content">
							<h4>HEMITO PORTFOLIO</h4>
							<p>Hemito International is established since 2000 in Qatar. It is an international service management company with a skilled workforce of more than 1000 employees consisting both males and females with over 10 years of experience.</p>
						</div>
						<div class="light-box">
							<a href="images/hemitoindex.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>

					<div class="single-gallery">
						<img class="img-fluid" src="images/RK-HANDICRAFTS-PORTFOLIO.png" alt="">
						<div class="gallery-content">
							<h4>RK HANDICRAFTS PORTFOLIO</h4>
							<p>RK Fine arts and handicrafts is a prominent leader in Portraying and handirats. Since its establishment in 2000, Rk has been providing extraordianry portraying services</p>
						</div>
						<div class="light-box">
							<a href="images/RK-HANDICRAFTS-PORTFOLIO.png" class="img-popup">
								<span class="lnr lnr-link"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="team-area">
	<div class="container">
		<div class="owl-carousel active-team-carusel">

						<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suman2.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suman Mali</h4>
								<p>Director | Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Suman Mali, An engineer graduate from Bangalore Technological Institute, who has been working on the front-end development using html, css, Laravel and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ashish1.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ashish Dulal</h4>
								<p>Designer | Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/ashishdulal052"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/coolboy_aziz"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Ashish Dulal, A developer graduate from Ambition College, who has been working on the front-end development using Photoshop, Html, CSS, Wordpress, Laravel and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>


			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/nakul4.JPG" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Nakul Budhathoki</h4>
								<p>Software Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/nakul.budhathoki"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>Our Interactive <br>
					Team Mates</h1>
					<p>
						Nakul Budhathoki, A developer graduate from Ambition College, who has been working on the Front-end development using wordpress, Laravel and javascript.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/Dipendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dipendra Manandhar</h4>
								<p>.Net Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dipen.manandhar.7"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dipendra Manandhar, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/ritendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ritendra Dahal</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dahalritendra" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ritendra Dahal, a designer graduated from Kantipur Engineering College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/kiipananda.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Er. Kripananda Yadav</h4>
								<p>SEO Specialist</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/kripananda772" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/kripananda772/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Kripananda Yadav, SEO specialist graduated Bachleors degree form Kantipur Engineering College, whos been handling overall work including techinical SEO and local SE.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/suroj.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Suroj Maharjan</h4>
								<p>Graphics Designer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/surose.maharjan.7" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Suroj Maharjan, a designer graduated from Designing College, who is been working in graphics design using various related software’s (InDesign, Photoshop, Illustrator) and also has good knowledge of html and css.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>
			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/matina.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Matina Manandhar</h4>
								<p>Front Desk Officer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/matina.manandhar.5" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="instagram.com/ma_tee_na/" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Matina Manandhar, front desk officer persuing Bachleors degree form Manmohan Memorial College,whos been handling overall work including finance and ecommerce.
					</p>
					<a class="primary-btn" href="/team">View Members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/Rajat-bhadra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Rajat-bhadra</h4>
								<p>Sales Executive</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Rajat-bhadra, Sales Executive who has been working for this company in London.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/dhirendra.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Dhirendra Kumar Yadav</h4>
								<p>Marketing Manager, Australia </p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="https://www.facebook.com/dhirendrakumaryadav"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/dhirendrakumaryadav"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Dhirendra is an experienced, determined and confident professional who has been working as a marketting Manager for this company in Australia (5 James Avenue Mitcham, 3132 , Victoria).
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/shyam-tamang.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Shyam Lal Tamang</h4>
								<p>.Net Developer</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Shyam Lal Tamang, A developer graduate from Khwopa Engineering College, who has been working on the .Net development using asp.net, mvc and c#.
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/RajuKArki.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Raju Karki</h4>
								<p>USA Branch</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Raju Karki, A developing patner residing in United States Of America (USA).
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>

			<div class="row align-items-center single-item">
				<div class="col-lg-6 col-md-6">
					<div class="team-left">
						<img class="img-fluid" src="images/AsheshDangal.jpg" alt="">
						<div class="member-desc d-flex justify-content-between align-items-center">
							<div class="m-title">
								<h4>Ashesh Dangol</h4>
								<p>USA Branch</p>
							</div>
							<div class="m-social">
								<div class="t-icons">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="b-icons">
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1 col-md-6 team-right">
					<h1>NTPL</h1>
					<p>
						Ashesh Dangol, A developing patner residing in United States Of America (USA).
					</p>
					<a class="primary-btn" href="/team">view members</a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="testimonial-area section-gap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 text-center">
				<div class="section-title">
					<p>Our Design & Development Services Provider</p>
					<h1>We Are <span>Nepgeeks.</span> Design & Development<br> <span>Services</span> Provider.</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-start">
				<div class="col-lg-12">
					<div class="brand-carousel owl-carousel">

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table">
							<div class="d-table-cell">
								<img src="images/lucentbusiness.png" alt="">
							</div>
						</div>

						<div class="single-brand-item d-table df-height">
							<div class="d-table-cell">
								<img src="images/dzinefolio.png" alt="">
							</div>
						</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="cta-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-12">
				<h2>Get to Know Project Estimate?</h2>
				<p>We have extensive experience in building applications ranging from web-based to mobile platform. We specialize in Web, Social Networking and Mobile platforms. </p>
				<a href="/contact" class="primary-btn">Get Estimate for Free</a>
			</div>
			<img src="images/cta-bg-shape.png" alt="" class="cta-shape img-fluid">
		</div>
	</div>
</section>


<section class="blog-area section-gap">
	<div class="container">
		<div class="row align-items-end justify-content-center">
			<div class="col-lg-5 col-md-12 text-left">
				<div class="section-title">
					<h1>Latest Posts<br> From Our Blog</h1>
					<p>Computer and network security: Everyone knows they should be doing it better, but no one really knows all the best ways to do it.</p>
				</div>
			</div>
		</div>
		<div class="row blog-inner justify-content-center">

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/h-blog1.jpg" alt="">
					</div>
					<div class="blog-details">
						<div class="blog-meta">
							<span>25 june, 2018 | By Ashish Dulal</span>
						</div>
						<h5><a href="#">SEO Friendly Web Design:<br>
						The Latest Web Design Trend</a></h5>
						<p>Designing a website is the first and foremost step in establishing a business online. At one hand, a business whereas it is equally important to get your business visible in this competitive inte....</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/h-blog2.jpg" alt="">
					</div>
					<div class="blog-details">
						<div class="blog-meta">
							<span>25 june, 2018 | By Suman Mali</span>
						</div>
						<h5><a href="#">Is Artificial Intelligence Gonna Be<br>
						The Biggest Thing in 2018? Scientists To Research AI Safely</a></h5>
						<p>Artificial Intelligence has been one of the biggest invention of the decade. It has completely changed the way we thought about programming & its capability. Just put some pieces of code into a machine and it can work for you as long as you ...</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-blog">
					<div class="blog-thumb">
						<img class="img-fluid" src="images/h-blog3.jpg" alt="">
					</div>
					<div class="blog-details">
						<div class="blog-meta">
							<span>25 june, 2018 | By Suresh Subedi</span>
						</div>
						<h5><a href="#">Here Is Why You Need A Dynamic Website<br>
						For Your Business In Nepal?</a></h5>
						<p>Website is one of the fundamental platforms for an online business.Whether you are a manufacturer, retailer or service provider, you are going to need a website to showcase your products and services in the digital world.A website acts as an online...</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection