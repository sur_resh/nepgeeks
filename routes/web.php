<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', 'AboutController@index');
Route::get('/service', 'ServiceController@index');
Route::get('/project', 'ProjectController@index');
Route::get('/project-details', 'ProjectController@projectdetail');
Route::get('/team', 'TeamController@index');
Route::get('/blog', 'BlogController@index');
Route::get('/contact', 'ContactController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/sendmail/subscribe', 'SendMailController@subscribe');

Route::get('/dashboard',function(){
	
		if(Auth::user()->admin == 1){
			$users['users'] = \App\User::all();
			return view('layouts.admin',$users);
		}
	});

Route::fallback(function() {
    return view('home');
});



Route::get('/portfolio', 'PortfolioController@index');
Route::get('/portfolio/create','PortfolioController@create');
Route::post('/portfolio/create','PortfolioController@store');
Route::get('/portfolio/edit/{id}','PortfolioController@edit');
Route::post('/portfolio/edit/{id}','PortfolioController@update');
Route::get('/portfolio/delete/{id}','PortfolioController@destroy');

