<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $arr['portfolio'] = Portfolio::paginate('5');
        return view('Portfolio.index')->with($arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('Portfolio.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $portfolio = new Portfolio();
        $portfolio->title = $request['title'];
         $portfolio->description = $request['description'];
          $portfolio->link = $request['link'];
       if(file_exists($request->image)){
            $filename = 'port'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/');
            $request->image->move($location, $filename);
            $portfolio->image = $filename;
        }
        else{
            $portfolio->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $portfolio->save();

       return redirect('/portfolio');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit( $id, Portfolio $portfolio)
    {
         $portfolio = Portfolio::findorFail($id);
        return view('Portfolio.edit',compact('portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $portfolio = Portfolio::findorFail($id);

        $portfolio->title = $request['title'];
        $portfolio->description = $request['description'];
          $portfolio->link = $request['link'];
       if(file_exists($request->image)){
            $file = 'port'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/');
            $request->image->move($location, $file);
            $portfolio->image = $file;
        }
        else{
            $portfolio->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $portfolio->save();

      

        return redirect('/portfolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio ,$id)
    {
        $portfolio = Portfolio::find($id)->delete();

       return redirect('/portfolio');
   }

}
