<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;	
use App\Mail\SendMail;

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'subject' =>  'required',
      'phone' =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'email'   =>   $request->email,
            'subject'   =>   $request->subject,
            'message'   =>   $request->message,
            'phone'   =>   $request->phone
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>'. $data['email'] .'<br></p>
                    <p>'. $data['phone'] .'<br></p>
                    <p>I have query for the subject: '. $data['subject'] .'</p>
                    <p>'.$data['message'].'</p>
                    <p>It would be appriciative, if i receive the reply soon.</p>
        </body>
        </html>';       

        $to = "nepgeeks@gmail.com";
        $Cc = "anil.qode@gmail.com";
        $subject = "Contact Request From nepgeeks site.";

        $headers = "From:nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }


    function subscribe(Request $request)
    {
     $this->validate($request, [
      'email'  =>  'required|email',
     ]);

        $sdata = array(
            'email'   =>   $request->email,
        );
        
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $sdata['email'] .'</p>
                    <p>I want to subscribe for the newsletter.</p>
                    <p>It would be appriciative, if i receive the subscription soon.</p>
        </body>
        </html>';       

        $to = "nepgeeks@gmail.com";
        $subject = "Subscription Request";

        $headers = "From:nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

         $result=   mail($to,$subject,$txt2,$headers);
                 return back()->with('success','Thanks for subscribing us!');
        }




}
